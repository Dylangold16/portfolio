var image = new Array("img/closeup.jpg","img/use.jpg","img/with.jpg","img/iholder.png","img/blueprint-1 copy.jpg","img/stool.jpg","img/nowig.jpg","img/manorah.jpg");
var description = new Array("Close up view of the 2D objects assembly into a larger 3D Manorah","Iholder - Iphone 5 headphone holding system", "Iholder designed to fit Iphone 5 and 5S","Iholder headphone system in Rhino 3D Software","Blueprints for the Iholder headphone system case","Handmade wooden stool with laser engraved image","Stool seat engraved using a CNC laser cutter with South Park meme of a banker saying, AAAAAAnd its Gone","CNC laser cut arcylic Manorah cut out of 2D acrylic and assembled into a 3D object");
var image_number = 0;
var image_length = image.length - 1;

function change_image(num) {
  image_number = image_number + num;
  if (image_number > image_length) {
    image_number = 0;
  }
  if (image_number < 0) {
    image_number = image_length;
  }
  document.slideshow.src=image[image_number];
  document.getElementById('description').innerHTML= description[image_number];
  return false;
}
